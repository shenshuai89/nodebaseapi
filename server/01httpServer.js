const http = require("http");
const url = require("url");
const querystring = require("querystring");
let server = http.createServer((req, res) => {
  let pathname = url.parse(req.url).pathname;
  console.log("pathname: " + pathname);

  let body = "";
  req.on("data", (data) => {
    body += data;
  });
  req.on("end", () => {
    res.writeHead(200, { "content-type": "text/plain" });
    if (req.method === "GET") {
      let query = querystring.parse(url.parse(req.url).query);
      console.log("query:%j", query);
      res.end("http-server-get");
    } else if (req.method === "POST") {
      let query = querystring.parse(body);
      console.log("query:%j", query);
      res.end("http-server-post");
    }
  });
});
server.listen(1234, () => {
  console.log("服务器启动监听localhost:1336");
});
