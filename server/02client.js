var options={
    hostname:"localhost",
    port:443,
    path:"/info?abc=123&name=China",
    method:"GET"
};
options.agent = new https.Agent(options);
//构造https请求（客户端）【自签名证书非法，应该使用正式正式】
var req=https.request(options,function(res)
{
    console.log("STATUS:"+res.statusCode);
    console.log("HEADERS:%j",res.headers);
    res.setEncoding("utf8");
    res.on("data",function(chunk)
    {
        console.log("response:"+chunk);
    });
});
req.end();//发送请求