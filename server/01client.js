const http = require("http");
const querystring = require("querystring");

const options = {
  hostname: "localhost",
  port: 1234,
  path: "/info/child?abc=123&name=china",
    method: "GET",
//   method: "POST",
};
const contents = querystring.stringify({
  name: "china",
  abc: 456,
});
const req = http.request(options, (res) => {
  console.log("status:" + res.statusCode);
  console.log("headers:" + res.headers);
  res.setEncoding("utf8");
  res.on("data", (data) => {
    console.log("response:" + data);
  });
});
// req.write(contents);
req.end();
