const https = require("https");
const url = require("url");
const fs = require("fs");
/* 
openssl genrsa -out privatekey.pem 1024 【生成privatekey.pem私钥】
openssl req -new -key privatekey.pem -out certrequest.csr 【生成certrequest.csr签名】
openssl x509 -req -in certrequest.csr -signkey privatekey.pem -out certificate.pem 【生成certificate.pem签名证书】
 */
var options = {
  key: fs.readFileSync("../privatekey.pem"),
  cert: fs.readFileSync("../certificate.pem"),
  passphrase: "123456", //你的密码
};
const server = https.createServer(options, function (req, res) {
  var pathname = url.parse(req.url).pathname;
  console.log("pathname:" + pathname);
  res.writeHead(200, { "Content-Type": "text/plain" });
  res.end("hello-server");
});
server.listen(443, () => {
  console.log("服务器监听localhost:443");
});
