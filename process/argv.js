const {argv, argv0, execArgv, execPath} = process;

// node argv.js a=123 b=344 
// argv表示执行文件时传递的参数
argv.forEach(item=>{
    console.log(item);
})

// execArgv 表示node命令和文件名直接的参数
// node --inspect argv.js a=123 b=344
console.log(execArgv); // ['--inspect']