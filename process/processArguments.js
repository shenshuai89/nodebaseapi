/*进程*/

var events = require("events");

console.log("主线程同步开始执行");

//argv参数
var argv = process.argv;
// console.log("程序执行的启动参数:",argv);
argv.forEach((arg, index) => {
  console.log(`参数${index}:${arg}`);
});

//当前进程id
console.log(`当前进程pid:${process.pid}`);
//当前工作目录
console.log(`当前工作目录:${process.cwd()}`);
//当前进程内存占用情况(单位字节)
console.log("当前进程内存占用:%j", process.memoryUsage());
//杀掉指定进程
// process.kill(process.pid,'SIGHUP');
//nextTick 每次事件轮询后，在额外的I/O执行前，next tick队列都会优先执行。 递归调用nextTick callbacks 会阻塞任何I/O操作，就像一个while(true); 循环一样。
process.nextTick(() => {
  console.log("nextTick-1执行");
});
process.nextTick(() => {
  console.log("nextTick-2执行");
});

//warning事件触发
process.on("warning", function (warning) {
  console.warn(warning.name); // 打印告警名称
  console.warn(warning.message); // 打印告警信息
  console.warn(warning.stack); // 打印堆栈信息
  console.warn(warning.code); // 打印警告唯一码
});
// events.defaultMaxListeners = 1;//设置默认最大的事件监听器数量
// process.on('foo', () => {});
// process.on('foo', () => {});
//超出最大监听器数量会触发进程警告!

//####自定义触发警告
// process.emitWarning("自定义警告！");
//或者通过Error传递一个警告
// var warningError=new Error("自定义警告错误对象");
// warningError.name="custom_error_warning";
// warningError.code="CW_1";
// process.emitWarning(warningError);
//####自定义触发警告结束

//退出之前事件
process.on("beforeExit", function (code) {
  // console.log("beforeExit事件触发");//打开会一直重复打印，致使程序关闭不了
});
//退出事件
process.on("exit", function (code) {
  console.log(`exit事件触发退出码：${code}`);
});

//进程运行的时长
console.log(`进程运行的时长(秒)：${process.uptime()}`);

// process.exit(11);//手动结束当前进程(并传递退出码)

console.log("主线程同步结束执行");

setTimeout(()=>{
    console.log('timer1')

    Promise.resolve().then(function() {
        console.log('promise1')
    })
}, 0)

setTimeout(()=>{
    console.log('timer2')

    Promise.resolve().then(function() {
        console.log('promise2')
    })
}, 0)
