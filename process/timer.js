
// 宏任务
setImmediate(() =>{
    console.log("setImmediate");
})

setTimeout(() =>{
    console.log("setTimeout");
}, 0)

// 微任务
process.nextTick(() =>{
    debugger
    console.log("nextTick");
})