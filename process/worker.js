const http = require("http");
const server = http.createServer((req, res) => {
  res.writeHead(200, {
    "Content-Type": "text/plain; charset=utf-8",
  });
  res.end("worker pid: " + process.pid + ", ppid: " + process.ppid);
  throw new Error("worker process exception!");
});

let worker;
process.title = "node-worker";
process.on("message", function (message, sendHandler) {
  if (message === "server") {
    worker = sendHandler;
    worker.on("connection", function (socket) {
      server.emit("connection", socket);
    });
  }
});
process.on("uncaughtException", function (err) {
  console.log(err);
  process.send({ act: "zidingyide" });
  // worker.close(function () {
  //   process.exit(1);
  // });
});
