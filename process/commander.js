// commander 处理命令行操作
const com = require("commander");

// 解析用户传入的参数，配置属性，解析命令中的静态参数
// node .\commander.js -p 3000 -v 1.0.1
com
  .option("-p, --port <val>", "set port")
  .option("-v, --vsion <val>", "set vsion")
  .version("1.2.3");
// console.log(com._optionValues.port, com._optionValues.vsion);

// 执行动作, 配置命令，输入命令后执行一些动作
com
  .command("create ")
  .alias(" c")
  .description("Create project")
  .action(() => {
    console.log(`git clone ${com.args[1]} project...`);
  });

com
  .on("--help", () => {
    console.log("\r\n Example");
    console.log("   node .commander.js create projectName");
    console.log("   node .commander.js --help");
  })
  .parse(process.argv);
// console.log(com.args[1]);
