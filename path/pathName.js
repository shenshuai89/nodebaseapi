const {basename, dirname, extname} = require("path");
const filePath = "/a/b/c/d/e/f.js"
console.log(basename(filePath)); // 文件名 f.js
console.log(dirname(filePath)); // 文件路径，不包括文件名 /a/b/c/d/e
console.log(extname(filePath)); // 文件后缀扩展名 .js