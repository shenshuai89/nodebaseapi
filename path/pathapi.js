const path = require("path");
let url = "/user/profile/note.md";

// 获取文件的父级路径，即除了最后一部分的路径内容
console.log(path.dirname(url));
// basename获取路径的最后一个部分
console.log(path.basename(url));
// 获取文件的后缀
console.log(path.extname(url));
// 使用basename，通过传递第二个参数，第二个参数为文件的后缀类型，可以获取到文件的名称
console.log(path.basename(url, path.extname(url)));

// path.join 连接两个路径或多个
const name = "node";
console.log(path.join("/ls", "users", name, "node.md"));
console.log(path.join("/a/b", "/c"));
console.log(path.join("/a/b", "/c", "../"));
console.log(path.join("/a/b", "/c", "../../"));


// resolve,获取当前路径下文件的绝对路径地址,遇到是绝对路径的参数时，直接输出绝对立即参数
console.log(path.resolve("node.md")); // /Users/shuai/Desktop/code/nodeapp/nodeapi/node.md
// 如果存在两个参数，第一个参数非/开头，则会把第一个参数拼接到绝对路径上
console.log(path.resolve("temp", "node.md")); // /Users/shuai/Desktop/code/nodeapp/nodeapi/temp/node.md
// resolve存在两个参数，如果第一个参数以/开头，则表示第一个参数就是绝对路径
console.log(path.resolve("/temp", "node.md")); // /temp/node.md
// resolve如果第二个参数是绝对路径，那么直接显示该绝对路径
console.log(path.resolve("/temp", "/node.md")); // /node.md
console.log('---------------');
// 解析路径,如果第二个是绝对路径，则会忽略第一个参数。
console.log(path.resolve("/foo/bar/temp", "./node.md"));  // /foo/bar/temp/node.md
console.log(path.resolve("/foo/bar/temp", "../node.md"));  //  /foo/bar/node.md
console.log(path.resolve("/foo/bar/temp", "/node.md"));  // /node.md

// normalize格式化路径, ../会查找上级路径，可以把node给过滤掉。
console.log(path.normalize("/users/node/..//api/test.txt"));

// 获取系统下，路径的分隔符
console.log(path.sep); // Linux下/，window下\

console.log(path.parse("/a/v/b/c/d/node.md"));

console.log(path.join(__dirname, '../new.md'));
