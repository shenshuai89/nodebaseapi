## puppeteer使用
### 安装
``` javascript
npm install puppeteer --save
```
由于安装过程需要下载Chromium，资源比较大，需要的时间比较长。
可以下载puppeteer-cn
``` javascript
npm install puppeteer-cn --save
```
[puppeteer-cn](https://npmmirror.com/package/puppeteer-cn)

### 官方案例，截屏
[文件链接](https://gitee.com/shenshuai89/nodebaseapi/blob/master/crawler/src/screen-shot.js)
``` javascript
const puppeteer = require('puppeteer-cn');
const path = require('path');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  page.setViewport({width:1920, height:1080});
  await page.goto('https://www.baidu.com');
  await page.screenshot({path: `${path.resolve(__dirname, '../images')}/baidu.png`});

  await browser.close();
})();
```
把百度首页进行截屏，并保存到images目录下。

### 百度搜索图片，并保存本地
[文件链接](https://gitee.com/shenshuai89/nodebaseapi/blob/master/crawler/src/getImage.js)

> 通过关键词搜索百度图片，把图片列表展示出来，然后通过fs写入到本地。
#### 找到按钮，模拟搜索
``` JavaScript
  await page.focus("#kw"); // 找到页面输入框的id
  await page.keyboard.sendCharacter("dog"); //给输入框输入字符
  await page.click(".s_newBtn"); //点击提交按钮
```
- 找到页面输入框Id
- 输入关键词
- 点击提交按钮

#### 获取图片的dom结构列表
在page.evaluate方法中，可以进行dom操作。
``` javascript
const imageUrls = await page.evaluate(() => {
  const images = document.querySelectorAll("img.main_img");
  return Array.from(images).map((image) => image.src);
});
// setImmediate可以控制在下一个队列的时候显示
setImmediate(() => {
  console.log("获取到图片的数量：", imageUrls.length);
});
imageUrls.forEach(async (url) => {
  // 连续请求之间添加时间间隔，避免触发反爬虫机制
  await page.waitForTimeout(100);
  await getImage(url, "../images");
});
```

### 获取book数据列表
[文件链接](https://gitee.com/shenshuai89/nodebaseapi/blob/master/crawler/src/book.js)
打开书籍列表网站，爬取书名和价格
``` javascript
// 打开页面
await page.goto("http://books.toscrape.com/", {
  waitUntil: "load",
  timeout: 0,
});

// 进行dom操作，出来数据集
const result = await page.evaluate(() => {
  let data = []; // 创建一个数组保存结果
  let elements = document.querySelectorAll(".product_pod"); // 选择所有书籍

  for (var element of elements) {
    // 遍历书籍列表
    let title = element.childNodes[5].innerText; // 提取标题信息
    let price = element.childNodes[7].children[0].innerText; // 提取价格信息

    data.push({ title, price }); // 组合数据放入数组
  }

  return data; // 返回数据集
});
```

### 获取头条数据并生成json
[文件链接](https://gitee.com/shenshuai89/nodebaseapi/blob/master/crawler/src/toutiao.js)
头条的数据是动态生成和加载，直接获取是拿不到dom数据。必须使用page.waitForSelector等待dom加载完成再进行后续操作。
``` javascript
await page.goto("https://www.toutiao.com/", {
  waitUntil: "load",
  timeout: 0,
});
await page.waitForSelector(".main-content"); // 非常重要，判断等一个节点加载完成后再往下执行
```

#### 为了获取更多数据，进行滚屏操作
``` JavaScript
await page.evaluate(() => {
  window.scrollBy(0, document.body.scrollHeight);
});
await page.waitForTimeout(1000);
```
#### 将数据写入json
``` javascript
fs.writeFileSync(
  path.join(__dirname, "./toutiao.json"),
  JSON.stringify(value),
  "utf8"
);
```

### 参考资源
[自动化爬虫入门](https://juejin.cn/post/6844903544919687181)
[Puppeteer常用API](https://juejin.cn/post/6844903997845962759)



