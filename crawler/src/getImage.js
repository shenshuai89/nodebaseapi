const puppeteer = require("puppeteer-cn");
const path = require("path");
const http = require("http");
const https = require("https");
const fs = require("fs");
const { promisify } = require("util");
const writeFile = promisify(fs.writeFile);

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto("https://image.baidu.com");
  //   await page.screenshot({path: `${path.resolve(__dirname, '../images')}/baidu.png`});
  console.log("image");
  await page.setViewport({ width: 1920, height: 1080 });
  console.log("view");

  await page.focus("#kw"); // 找到页面输入框的id
  await page.keyboard.sendCharacter("dog"); //给输入框输入字符
  await page.click(".s_newBtn"); //点击提交按钮
  console.log("click");

  page.on("load", async () => {
    console.log("page loading, start fetch");
    const imageUrls = await page.evaluate(() => {
      const images = document.querySelectorAll("img.main_img");
      return Array.from(images).map((image) => image.src);
    });
    setImmediate(() => {
      console.log("获取到图片的数量：", imageUrls.length);
    });
    imageUrls.forEach(async (url) => {
      console.log(url);
      // 连续请求之间添加时间间隔，避免触发反爬虫机制
      await page.waitForTimeout(100);
      await getImage(url, "../images");
    });
    await browser.close();
  });
})();

const getImage = async function (url, dir) {
  if (url.includes("baidu.com") || /\.(jpg|png|gif)$/.test(url)) {
    await urlToImg(url, dir);
  } else {
    await base64ToImg(url, dir);
  }
};
// 链接是url的转图片
const urlToImg = promisify((url, dir) => {
  const mod = /^https:/.test(url) ? https : http;
  //   const ext = path.extname(url);
  // 通过正则匹配获取f=到？之间的数据作为文件后缀。
  const ext = /f\=(.+?)\?/.test(url)
    ? url.match(/f\=(.+?)\?/)[1].toLowerCase()
    : "jpg";
  const file = path.join(
    path.resolve(__dirname, dir),
    `${Date.now() + "-" + parseInt(Math.random() * 1000)}.${ext}`
  );
  // 获取http.get或者https.get方法
  mod.get(url, (res) => {
    res.pipe(fs.createWriteStream(file)).on("finish", () => {});
  });
});
// 链接是base64的转图片
const base64ToImg = async (base64Data, dir) => {
  // data:image/jpeg;base64,
  const matches = base64Data.match(/^data:(.+?);base64,(.+)$/);
  try {
    const ext = matches[1].split("/")[1].replace("jpeg", "jpg");
    const file = path.join(
      path.resolve(__dirname, dir),
      `${Date.now() + "-" + parseInt(Math.random() * 1000)}.${ext}`
    );
    // 调用fs.writeFile方法将base64数据写入磁盘
    await writeFile(file, matches[2], "base64");
  } catch (e) {
    console.log(e, "非法的base64格式数据");
  }
};
