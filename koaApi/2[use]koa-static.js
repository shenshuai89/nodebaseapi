const koa = require("koa");
const app = new koa();
const fs = require("fs");
const path = require("path");
// const bodyparser = require("koa-bodyparser");
const bodyparser = require("./body-parser");
const static = require("./static");
const koaStatic = require("koa-static");
app.use(static(__dirname));
app.use(static(path.resolve(__dirname, "upload")));
app.use(bodyparser());

app.use(async (ctx, next) => {
  if (ctx.path === "/form" && ctx.method === "GET") {
    ctx.set("Content-Type", "text/html;charset=utf-8");
    ctx.body = fs.createReadStream("./form.html");
  } else {
    await next();
  }
});
app.use((ctx, next) => {
  if (ctx.path === "/login" && ctx.method === "POST") {
    ctx.body = ctx.request.body;
    // 可以封装成bodyparser中间件
    // return new Promise((resolve, reject) => {
    //   let arr = [];
    //   ctx.req.on("data", (data) => {
    //     arr.push(data);
    //   });
    //   ctx.req.on("end", () => {
    //     ctx.body = Buffer.concat(arr).toString();
    //     resolve();
    //   });
    // });
  }
});

app.listen(3000);
