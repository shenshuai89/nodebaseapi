const { createReadStream } = require("fs");
const fs = require("fs").promises;
const path = require("path");
const mime = require("mime");
function static(dirname) {
  return async (ctx, next) => {
    try {
      let filePath = path.join(dirname, ctx.path);
      let statObj = await fs.stat(filePath);
      if (statObj.isDirectory()) {
        filePath = path.join(filePath, "index.html");
        await fs.access(filePath);
      }
      ctx.set("Content-Type", mime.getType(filePath) + ";charset=UTF-8");
      ctx.body = createReadStream(filePath);
    } catch (e) {
      console.log(e);
      await next();
    }
  };
}

module.exports = static;
