/* 打开文件 */
const fs = require("fs");
const path = require("path");
// fs.open 参数：(path, flags[, mode], callback)
fs.open(path.join(__dirname, "./x.txt"), "r", function (err, rfd) {
  if (err) throw err;
  console.log("file open success");
  const AB = new ArrayBuffer(1024);
  var buf = Buffer.from(AB);
  // 方法原型:  fs.read(fd, buffer, offset, length, position, callback)
  fs.read(rfd, buf, 0, buf.length, 0, function (err, bytes) {
    if (err) {
      fs.closeSync(rfd);
      throw err;
    }
    console.log("file size bytes" + bytes);
    if (bytes > 0) {
      console.log("打开文件读取的buffer数据"+ buf.slice(0, bytes).toString());
    }
    fs.closeSync(rfd);
  });
});
