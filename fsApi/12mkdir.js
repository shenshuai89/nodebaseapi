const fs = require("fs");
const path = require("path");

fs.mkdir("new", function (err) {
  if (err) throw err;
  console.log("mkdir ok");
});

setTimeout(() => {
  fs.rmdir("new", function (err) {
    if (err) throw err;
    console.log("rmdir ok");
  });
}, 2000);
