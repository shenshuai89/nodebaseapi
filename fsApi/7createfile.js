const fs = require("fs");
const path = require("path");

if (!fs.existsSync(path.join(__dirname, "test"))) {
  //判断文件不存在
  // 创建目录
  fs.mkdir(path.join(__dirname, "test"), function (err) {
    if (err) throw err;
    console.log("test dir create success");
    /* write file */
    fs.writeFile(
      path.join(__dirname, "test", "fs_write.txt"),
      "hello world",
      function (err) {
        if (err) throw err;
        console.log("write file success");
      }
    );
  });
}
