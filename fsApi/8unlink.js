const fs = require("fs");
const path = require("path");

// fs.unlink 删除文件
// fs.rmdir 删除目录
// 删除目录需要先删除文件
if (fs.existsSync(path.join(__dirname, "test"))) {
  fs.unlink(path.join(__dirname, "test", "fs_write.txt"), function (err) {
    if (err) throw err;
    console.log("删除文件成功");
    // 删除目录
    fs.rmdir(path.join(__dirname, "test"), function (err) {
      if (err) throw err;
      console.log("删除目录成功");
    });
  });
}
