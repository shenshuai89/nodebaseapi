const fs = require("fs");
const path = require("path");
// fs.stat可以用来判断文件是否存在，尽量不用同步方法 existsAsync 因为同步方法会对高并发场景有阻塞问题。
fs.stat(path.join(__dirname, "test/fs_write.txt"), function (err, stats) {
  if (err) throw err;
  console.log(stats);
  console.log("判断读取的是否是文件" + stats.isFile());
  console.log("判断读取的是否是目录" + stats.isDirectory());
});
fs.stat(path.join(__dirname, "test"), function (err, stats) {
  if (err) throw err;
  console.log(stats);
  console.log("判断读取的是否是文件" + stats.isFile());
  console.log("判断读取的是否是目录" + stats.isDirectory());
});