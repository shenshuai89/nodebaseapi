const fs = require("fs");
const path = require("path");
// readdir 读取目录下的内容
/* fs.readdir(path.join(__dirname, "a"), function (err, dirs) {
  dirs = dirs.map((d) => path.join(__dirname, "a", d));
  console.log(dirs);
  dirs.forEach((dir) => {
    // 判断当前路径的状态
    fs.stat(dir, function (err, statObj) {
      if (err) return;
      if (statObj.isFile()) {
        fs.unlink(dir, (err) => {});
      } else {
        fs.rmdir(dir, (err) => {
          if (!err) {
            console.log(`删除目录${dir}成功`);
          }
        });
      }
    });
  });
}); */
// const path = require("path");
// console.log(path.join('/Users/shuai/Desktop/code/nodeapp/nodeapi/fsApi','/Users/shuai/Desktop/code/nodeapp/nodeapi/fsApi/a/1.js'));
// rmdir删除目录
// 先序 深度, 串行删除
function preDeepRemoveDirectory(dir, callback) {
  fs.stat(path.resolve(__dirname, dir), function (err, statObj) {
    if (statObj && statObj.isFile()) {
      // 路径是文件
      fs.unlink(dir, callback);
    } else {
      // 路径是目录
      fs.readdir(path.resolve(__dirname, dir), (err, dirs) => {
        // dirs是目录下的所有文件和目录[b, c, 1.js]
        // 处理目录的路径问题[a/b, a/c, a/1.js]
        dirs = dirs.map((item) => path.resolve(__dirname, dir, item));
        let index = 0;
        function next() {
          //通过设置条件，判断什么时候删除根
          if (index === dirs.length) {
            return fs.rmdir(path.resolve(__dirname, dir), callback);
          }
          // 递归是将index++，进入下一个路径
          let currentPath = dirs[index++];
          preDeepRemoveDirectory(currentPath, next);
        }
        next();
      });
    }
  });
}

preDeepRemoveDirectory("a", () => {
  console.log("删除文件成功");
});

/* // rmdir删除目录
// 先序 深度, 并行删除
function preParallDeepRemoveDirectory(dir, callback) {
  fs.stat(path.resolve(__dirname, dir), function (err, statObj) {
    // console.log(statObj);
    // 先判断传入的路径是文件还是目录
    if (statObj.isDirectory()) {
      //是目录
      // 读取目录下的内容
      fs.readdir(path.resolve(__dirname, dir), function (err, dirs) {
        // console.log(dirs); //[ 'a.js', 'b' ]
        // 处理目录下文件的路径
        dirs = dirs.map((item) => path.resolve(__dirname, dir, item));
        // console.log(dirs);//[a/a.js,a/b]
        // 如果dirs读取目录的结果为空，没有子文件或目录，要把自己当前目录删除
        if (dirs.length === 0) {
          return fs.rmdir(path.resolve(__dirname, dir), callback);
        }
        // 并行处理程序，需要建立一个都要执行的函数，设置执行的次数，来判断终止条件
        let index = 0;
        const done = () => {
          index++;
          if (index === dirs.length) {
            return fs.rmdir(path.resolve(__dirname, dir), callback);
          }
        };

        dirs.forEach((dir) => {
          // 并行 删除目录下的所有子目录
          preParallDeepRemoveDirectory(dir, done);
        });
      });
    } else {
      //是文件
      fs.unlink(path.resolve(__dirname, dir), callback);
    }
  });
}
preParallDeepRemoveDirectory("a", () => {
  console.log("删除文件成功");
}); */

/* // promise版本。先序 深度 并行删除
function preParallDeepPromise(dir) {
  return new Promise((resolve, reject) => {
    fs.stat(path.resolve(__dirname, dir), function (err, statObj) {
      // console.log(statObj);
      // 先判断传入的路径是文件还是目录
      if (statObj.isDirectory()) {
        //是目录
        // 读取目录下的内容
        fs.readdir(path.resolve(__dirname, dir), function (err, dirs) {
          // console.log(dirs); //[ 'a.js', 'b' ]
          // 处理目录下文件的路径
          dirs = dirs.map((item) => preParallDeepPromise(path.resolve(__dirname, dir, item)));
          // console.log(dirs);//[a/a.js,a/b]
          Promise.all(dirs).then(() => {
            fs.rmdir(path.resolve(__dirname, dir), resolve);
          });
        });
      } else {
        //是文件
        fs.unlink(path.resolve(__dirname, dir), resolve);
      }
    });
  });
}
preParallDeepPromise("a").then(()=>{
  console.log("删除文件成功");
}); */

/* // 使用async + await
let {unlink, readdir, stat, rmdir} = require("fs").promises;
async function preParallDeepAsync(dir){
  let statObj = await stat(path.resolve(__dirname, dir));
  if(statObj.isFile()){
    await unlink(path.resolve(__dirname, dir));
  }else{
    let dirs = await readdir(path.resolve(__dirname, dir));
    dirs = dirs.map(item => preParallDeepAsync(path.resolve(__dirname, dir, item)))
    await Promise.all(dirs);
    await rmdir(path.resolve(__dirname, dir))
  }
}
preParallDeepAsync("a").then(()=>{
  console.log("删除文件成功");
}); */

// 广度遍历树
function wide(dir) {
  let arr = [path.resolve(__dirname, dir)];
  let index = 0,
    current;
  while ((current = arr[index++])) {
    let statObj = fs.statSync(path.resolve(__dirname, current));
    if (statObj.isDirectory()){
      let dirs = fs.readdirSync(path.resolve(__dirname, current));
      // console.log(dirs);
      dirs = dirs.map((item) => path.resolve(__dirname, current, item));
      arr = [...arr, ...dirs];
    }
  }
  return arr;
}
console.log(wide("a"));
