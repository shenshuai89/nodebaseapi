const fs = require("fs");
const path = require("path");
// fs对文件的操作
// readFile writeFile existsSync【判断路径存在则返回true，否则返回false】  access【用户对path指定的文件或目录的权限】

// fs对目录的操作 operationDirectory
// 创建多层目录
function loopCreateDirectory(paths, callback) {
  // 分割路径
  paths = paths.split("/");
  let index = 0;
  function loop() {
    if (index === paths.length) {
      return callback();
    }
    let dirPath = path.join(__dirname, paths.slice(0, ++index).join("/"));
    // 判断要创建的目录是否存在
    fs.access(dirPath, function (err) {
      if (err) {
        // 出错说明目录不存在
        fs.mkdir(dirPath, loop);
      } else {
        loop();
      }
    });
  }
  loop();
}
loopCreateDirectory("a/b/c/d/e/f", (err) => {
  if (!err) {
    console.log("创建成功");
  }
});

