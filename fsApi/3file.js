const fs = require("fs");
const path = require("path");
// unlink删除文件
fs.unlink(path.join(__dirname, "1.md"), (err) => {
  if (!err) {
    console.log("删除文件成功");
  }
});
// rename重命名文件
fs.rename(path.join(__dirname, "note.txt"), path.join(__dirname, "x.txt"), (err) => {
  if (!err) {
    console.log("重命名文件成功");
  }
});

