const fs = require('fs');
const path = require('path');

// fs读取文件内容和写文件，不适合操作大文件，会造成内存浪费。

// readFile读取文件,writeFile写内容到文件
fs.readFile(path.join(__dirname, '1.txt'), function(err, data) {
    if(err){
        console.log(err);
    }
    fs.writeFile(path.join(__dirname, '1.md'), data, function(err, data) {
        if(!err){
            console.log('write ok');
        }
    })
})

// fs主要api: fs.open、fs.read、fs.write、fs.close
let buffer = Buffer.alloc(10)
fs.open(path.join(__dirname, '1.txt'), 'r', function(err, rfd){ // read file descriptor
    // read rfd表示文件; buffer 读取的内容写入哪个buffer中
    // 0,3 从buffer的0开始写，写入3个
    // 0 表示开始读取数据的位置
    fs.read(rfd, buffer, 0, 3, 0, function(err, bytesRead){ // bytesRead真正读取的数据
        console.log('object', bytesRead);
    })
})

// write数据
fs.open(path.join(__dirname, '1.txt'), 'r', function(err, rfd){ // read file descriptor
    fs.open(path.join(__dirname, 'note.txt'), 'w', function(err, wfd){
        fs.read(rfd, buffer, 0, 3, 0, function(err, bytesRead){ // bytesRead真正读取的数据
            //  只写入了3个数据
            fs.write(wfd, buffer, 0, 3, 0, function(err){
                console.log("write ok");
            })
        })
    })
})

// read和write数据存在强耦合操作，一般使用流steam操作