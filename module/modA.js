// node的commonjs 只会执行已加载的部分
/* 模块被循环加载，只输出已执行部分，未执行的不会输出 */
module.exports.test = "A";
const modB = require("./modB");
console.log('modA: ', modB.test);

module.exports.test = "AA"