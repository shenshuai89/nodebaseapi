const proto = {
  //   get url() {
  //     return this.request.url;
  //   },
  //   get path() {
  //     return this.request.path;
  //   },
};
function delegate(target, key) {
  proto.__defineGetter__(key, function () {
    return this[target][key];
  });
}
function delegateSetter(target, key) {
  proto.__defineSetter__(key, function (value) {
    this[target][key] = value;
  });
}
delegate("request", "url");
delegate("request", "path");
delegate("request", "query");
delegate("response", "body");

delegateSetter("response", "body");
module.exports = proto;
