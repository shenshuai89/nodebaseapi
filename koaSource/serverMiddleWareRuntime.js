const Koa = require("koa");
const app = new Koa();
const logger = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("logger");
      resolve();
    }, 1000);
  });
};
app.use(async (ctx, next) => {
  console.log(1);
   next();
  ctx.body = "111";
  console.log(2);
});

app.use(async (ctx, next) => {
  console.log(3);
  await logger();
   next();
  ctx.body = "222";
  console.log(4);
});
app.use(async (ctx, next) => {
  console.log(5);
  await next();
  ctx.body = "333";
  console.log(6);
});
// 不打开logger时
// next都添加await时，返回1，3，5，6，4，2【body：111】
// next都不添加await时，返回1，3，5，6，4，2【body：111】
// next只在第1个use加await时，返回1，3，5，6，4，2【body：111】
// next只在第2个use加await时，返回1，3，5，6，2，4【body：222】
// next只在第3个use加await时，返回1，3，5，4，2，6【body：333】

// 打开logger
// next都添加await时，返回1，3，logger，5，6，4，2【body：111】
// next都不添加await时，返回1，3，2，logger，5，6，4【body：111】
// next只在第1个use加await时，返回1，3，logger，5，6，4，2【body：111】
// next只在第2个use加await时，返回1，3，2，logger，5，6，4【body：111】
// next只在第3个use加await时，返回1，3，2，logger，5，4，6【body：111】

app.listen(3000);
