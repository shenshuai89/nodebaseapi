const Koa = require("./koa");
const app = new Koa();
const fs = require("fs");
const logger = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("logger");
      resolve();
    }, 1000);
  });
};
app.use(async (ctx, next) => {
  console.log(1);
  next();
  console.log(2);
  ctx.body = "111";
  // ctx.body = fs.createReadStream("./server.js")
});

app.use(async (ctx, next) => {
  console.log(3);
  // await logger();
  next();
  console.log(4);
  ctx.body = "222";
});
app.use(async (ctx, next) => {
  console.log(5);
  await next();
  console.log(6);
  ctx.body = "333";
});

app.listen(3000);
