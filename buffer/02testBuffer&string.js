const http = require("http");
let s = "";
for (let i = 0; i < 1024 * 10; i++) {
  s += "a";
}

const str = s;
const bufStr = Buffer.from(s);
const server = http.createServer((req, res) => {
  console.log(req.url);

  if (req.url === "/buffer") {
    res.end(bufStr);
  } else if (req.url === "/string") {
    res.end(str);
  }
});

server.listen(3000);

// 是AB测试，可以看出buffer比string快速排序
// ab -c 200 -t 60 http://10.21.103.34:3000/buffer
// ab -c 200 -t 60 http://10.21.103.34:3000/string