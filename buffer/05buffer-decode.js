const { StringDecoder } = require("string_decoder");
const decoder = new StringDecoder("utf8");

const buf = Buffer.from("我爱中国！");

for (let i = 0; i < buf.length; i += 5) {
  const b = Buffer.alloc(5);
  buf.copy(b, 0, i);
  console.log(b.toString()); // 乱码
}
console.log('----=-=-======-----------');
for (let i = 0; i < buf.length; i += 5) {
  const b = Buffer.alloc(5);
  buf.copy(b, 0, i);
  console.log(decoder.write(b)); // 乱码
}
