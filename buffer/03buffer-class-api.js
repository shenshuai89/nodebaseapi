/*
byteLength: 多少个字节
isBuffer: 是否是buffer对象
concat :  拼接buffer
 */
console.log(Buffer.byteLength("hello"));
console.log(Buffer.byteLength("你好"));

console.log(Buffer.isBuffer({}));
console.log(Buffer.isBuffer(Buffer.from([1, 2])));

let buf1 = Buffer.from('hello')
let buf2 = Buffer.from('everyone')
let buf3 = Buffer.from('!!!')
let bufMain = Buffer.concat([buf1, buf2, buf3]); // concat 拼接多个buffer
console.log(bufMain.toString('utf8')); // toString 将buffer按照某种规范打印出来