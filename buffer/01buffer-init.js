// Buffer.from  first argument must be of type string or an instance of Buffer, ArrayBuffer, or Array
let buf = Buffer.from("hello");
let bufArr = Buffer.from([1,2,3]);
let buf1 = Buffer.alloc(10);

console.log(buf, bufArr, buf1);
let bufFill1 = Buffer.alloc(10, 1);
console.log(bufFill1);

// allocUnsafe填充的值是随机不确定的
console.log(Buffer.allocUnsafe(10,'a'));
