/* 
    length
    toString
    fill
    equals
    indexOf
    copy
*/
let buf = Buffer.from("test buffer instance api");
console.log(buf.length);
console.log(buf.toString("base64"));
console.log(buf.fill(1));

let buf1 = Buffer.from("test");
let buf2 = Buffer.from("test");
console.log(buf1.equals(buf2));
let buf3 = Buffer.from("test!");
console.log(buf1.equals(buf3));

console.log(buf3.indexOf('es'));
console.log(buf3.indexOf('ea'));
