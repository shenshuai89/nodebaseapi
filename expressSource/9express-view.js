const express = require("express");
const path = require("path");
const app = express();

// 修改默认views的解析路径，将默认的views改为ejs
app.set("views", path.join(__dirname, "ejs"));
// 设置html类型的文件
app.set("views engine", "html");
// html类型的文件，使用ejs模版解析
app.engine("html", require("ejs").__express);
console.log(app.get("views"));
app.get("/", function (req, res, next) {
  // 默认查看的是views文件下
  res.render("hello.html", { name: "world" });
});

app.listen(3000);
