const express = require("./express");
const app = express();
const user = require("./routers/user");
const article = require("./routers/article");

app.use("/user", user);
app.use("/article", article);
app.listen(3000);
