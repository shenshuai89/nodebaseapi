const express = require("./express");
const app = express();

app.get("/", function (req, res) {
  res.end("home");
});

app.get("/hello", function (req, res) {
  res.end("hello");
});

// app.all("*", function (req, res) {
//   console.log("all");
//   res.end("404")
// });

app.listen(3000);
