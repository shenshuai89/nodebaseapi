const express = require("express");
const app = express();

app.use(function (req, res, next) {
  const startTime = Date.now();
  let stashAPI = res.end;
  res.end = function (value) {
    let endTime = Date.now();
    console.log(endTime - startTime);
    stashAPI.call(res, value);
  };
  next();
});

app.get("/", function (req, res) {
  setTimeout(function () {
    res.end("home");
  }, 1000);
});
app.get("/user", function (req, res) {
  setTimeout(function () {
    res.end("user");
  }, 2000);
});
app.listen(3000);
