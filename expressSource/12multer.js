const express = require("express");
const app = express();
const fs = require("fs");
const path = require("path");

const multer = require("multer");
const upload = multer({ dest: "uploads/" });
// 将ejs文件夹设置为静态目录，打开upload.html文件
app.use(express.static(path.join(__dirname, "ejs")));
app.post("/upload", upload.single("avatar"), function (req, res) {
  fs.rename(
    path.join(__dirname, "/uploads/", req.file.filename),
    path.join(__dirname, "/uploads/", Date.now() + "_" + req.file.originalname),
    (err) => {
      if (err) {
        throw err;
      } else {
        res.send(req.file);
      }
    }
  );
});

app.listen(3000);
