const express = require("./express");
const app = express();
console.log(app.use, "app.use");
app.use(function (req, res, next) {
  console.log(1);
  req.a = 1;
  next();
});

app.use("/", function (req, res, next) {
  console.log(2);
  req.a++;
  next();
});

app.use("/a", function (req, res, next) {
  console.log(3);
  req.a++;
  next();
});

app.get("/a", function (req, res, next) {
  res.end(req.a + "");
});
app.get("/", function (req, res, next) {
  res.end(req.a + "");
});

app.listen(3000);
