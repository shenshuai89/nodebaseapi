const express = require("./express");
const app = express();

// app.get("/", function (req, res) {
//   res.end("home");
// });

app.get("/", function (req, res, next) {
  console.log(1);
  next();
}, function (req, res, next) {
  console.log(2);
  next();
}, function (req, res, next) {
  console.log(3);
  next();
}, function (req, res, next) {
  console.log(4);
  next();
});

app.post("/", function (req, res, next) {
  console.log("post end");
  res.end("post ok")
})

app.get("/hello", function (req, res, next) {
  console.log("end");
  res.end("hello")
})

// app.all("*", function (req, res) {
//   console.log("all");
//   res.end("404")
// });

app.listen(3000);
