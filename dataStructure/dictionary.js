class Dictionary {
  constructor() {
    this.items = {};
  }
  set(key, value) {
    this.items[key] = value;
  }
  get(key) {
    return this.has(key) ? this.items[key] : undefined;
  }
  has(key) {
    return this.items.hasOwnProperty(key);
  }
  keys() {
    return Object.keys(this.items);
  }
  values() {
    return Object.values(this.items);
  }
  size() {
    return this.keys().length;
  }
  remove(key) {
    if (!this.has(key)) return false;
    delete this.items[key];
    return true;
  }
  clear() {
    this.items = {};
  }
}

module.exports = Dictionary;
