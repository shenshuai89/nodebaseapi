// 队列
class Queue {
  constructor() {
    this.queue = [];
  }
  // 给队列添加元素
  enqueue(item) {
    this.queue.push(item);
  }
  // dequeue删除队列的头部
  dequeue() {
    return this.queue.shift();
  }
  // 查看队列头部
  front() {
    return this.queue[0];
  }
  isEmpty() {
    return this.queue.length === 0;
  }
  size() {
    return this.queue.length;
  }
  toString() {
    return this.queue.toString();
  }
}
module.exports = Queue;

let queue = new Queue();
// 在队列中添加元素
queue.enqueue("abc");
queue.enqueue("cba");
queue.enqueue("nba");
console.log(queue.toString());

// 击鼓传花是一个常见的面试算法题,可以使用队列的数据结构处理
// 第一个人从1报数，喊到指定数的退出
function passGame(names, decNumer) {
  let queue = new Queue();
  // 将成员添加到队列中
  names.forEach((name) => queue.enqueue(name));
  // 通过while寻找最后剩下的人
  while (queue.size() > 1) {
    for (let i = 0; i < decNumer - 1; i++) {
      queue.enqueue(queue.dequeue());
    }
    // 这里删除头部，即将decNumer个人从队列中移除
    queue.dequeue();
  }
  // console.log(queue.size())
  //  队列剩下最后一个人，取出最后一个元素返回
  var endName = queue.dequeue();
  // console.log("最终留下来的人:" + endName)
  return endName;
}

var names = ["John", "Jack", "lili", "lucy", "jim"];
var lastMember = passGame(names, 5); // 数到8的人淘汰
console.log("最终位置:" + lastMember);

// 用数组的操作实现
function lastMemberGame(teams, num) {
  while(teams.length > 1){
    for (var i = 0; i < num - 1; i++) {
      teams.push(teams.shift());
    }
    teams.shift();
  }
  return teams[0];
}
console.log(lastMemberGame(names, 5));
