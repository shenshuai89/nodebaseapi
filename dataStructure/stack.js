// 定义栈的数据结构
class Stack {
  constructor() {
    this.stack = [];
  }
  // 压入栈中
  push(item) {
    this.stack.push(item);
  }
  // 出栈操作,会返回被删除的元素
  pop() {
    return this.stack.pop();
  }
  // peek操作查看栈顶的数据,不删除数据
  peek() {
    return this.stack[this.stack.length - 1];
  }
  isEmpty() {
    return this.stack.length == 0;
  }
  size() {
    return this.stack.length;
  }
  toString() {
    return this.stack.toString();
  }
}
let stack = new Stack();

// 情况下代码模拟
stack.push(6);
stack.push(5);
console.log(stack.pop()); // 5
stack.push(4);
console.log(stack.pop()); // 4
stack.push(3);
console.log(stack.pop()); // 3
console.log(stack.pop()); // 6
stack.push(2);
stack.push(1);
console.log(stack.pop()); // 1
console.log(stack.pop()); // 2

// 封装十进制转二进制的函数
function dec2bin(decNumer) {
  // 定义变量
  var stack = new Stack();
  var remainder;

  // 循环除法
  while (decNumer > 0) {
    remainder = decNumer % 2;
    decNumer = Math.floor(decNumer / 2);
    stack.push(remainder);
  }

  // 将数据取出
  var binayriStrng = "";
  while (!stack.isEmpty()) {
    binayriStrng += stack.pop();
  }
  return binayriStrng;
}
console.log(dec2bin(100));
