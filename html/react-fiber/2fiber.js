/* 
Fiber是一个执行单元
    1. Fiber是一个执行单元，类似对象。每次执行完一个执行单元，react会检查可使用的时间还有多少，如果没时间就把控制权让出去
    2. 通过Fiber架构，让Reconcilation过程变成可被中断，适时会让出CPU执行权，让浏览器优先处理用户的交互
Fiber执行阶段
    每次渲染包含2个阶段：Reconcilation(协调或render渲染)和Commit(提交阶段)
    协调阶段：Reconcilation可以认为是diff阶段，该阶段可被中断，会造成节点变更，如新增、删除等，这些变更react称为副作用effect
    提交阶段：将上一个阶段计算出来的所有需要处理的副作用一次性执行。这个阶段必须同步执行，不能被打断
 */
let A1 = { type: "div", key: "A1" };
let B1 = { type: "div", key: "B1", return: A1 };
let B2 = { type: "div", key: "B2", return: A1 };
let C1 = { type: "div", key: "C1", return: B1 };
let C2 = { type: "div", key: "C2", return: B1 };
A1.child = B1;
B1.sibling = B2;
B1.child = C1;
C1.sibling = C2;

let nextUnitOfWork; // 下一个执行单元
function workLoop() {
  while (nextUnitOfWork) {
    nextUnitOfWork = performUnitOfWork(nextUnitOfWork);
  }
  if (!nextUnitOfWork) {
    console.log("render阶段结束");
  }
}
function performUnitOfWork(fiber) {
  beginWork(fiber);
  if (fiber.child) {
    //如果有子，返回第一个子节点
    return fiber.child;
  }
  while (fiber) {
    completeUnitOfWork(fiber);
    if (fiber.sibling) {
      return fiber.sibling;
    }
    fiber = fiber.return;
  }
}
function completeUnitOfWork(fiber) {
  console.log(fiber.key, "end");
}
function beginWork(fiber) {
  console.log(fiber.key, "start");
}
nextUnitOfWork = A1;
workLoop();
