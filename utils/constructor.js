function Parent() {}
Parent.prototype.pMethod = function pMethod() {
  console.log("Parent Method Fn");
};
function Child() {}
// 子对象的原型引用父对象的原型
Child.prototype = Object.create(Parent.prototype);
// 重置下Child对象的构造函数
Child.prototype.constructor = Child;
Child.prototype.ChildCreate = function () {
  // 如果重置Child的构造函数，则this.constructor为Child；如果没有重置constructor，则this.constructor为Parent
  console.log("ChildCreate Fn", this.constructor);
  return new this.constructor();
};
let child = new Child();
// 没有重置constructor只能调用Parent上的方法
child.ChildCreate().pMethod();
// 重置constructor后，可调用Parent上的方法也可调用Child的方法
child.ChildCreate().ChildCreate();

function ParentWithStatic() {}
ParentWithStatic.startPosition = { x: 0, y: 0 };
ParentWithStatic.getStartPosition = function getStartPosition() {
  console.log(this.startPosition, "this.startPosition");
  return this.startPosition;
};

function ChildPosition(x, y) {
  this.position = {
    x: x,
    y: y,
  };
}

ChildPosition.prototype = Object.create(ParentWithStatic.prototype);
// 如果重置constructor，下面使用getStartPosition方法就会报错
// ChildPosition.prototype.constructor = ChildPosition;

ChildPosition.prototype.getOffsetByInitialPosition = function () {
  var position = this.position;
  // 此处的this.constructor是ParentWithStatic
  var startPosition = this.constructor.getStartPosition(); // error undefined is not a function, since the constructor is Child
  // var startPosition = ParentWithStatic.prototype.getStartPosition(); // error undefined is not a function, since the constructor is Child

  return {
    offsetX: startPosition.x - position.x,
    offsetY: startPosition.y - position.y,
  };
};
let cp = new ChildPosition(1, 2);
console.log(cp.getOffsetByInitialPosition());
