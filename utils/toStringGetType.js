
var class2type = {};

// 生成class2type映射
"Boolean Number String Function Array Date RegExp Object Error".split(" ").map(function(item, index) {
    class2type["[object " + item + "]"] = item.toLowerCase();
})
/**
 * @description: 是toString进行类型判断
 * @param {*} obj，需要判断的对象
 * @return {*}
 */
function type(obj) {
    // 兼容IE6 null 和 undefined 会被 Object.prototype.toString 识别成 [object Object]
    if (obj == null) {
        return obj + "";
    }
    return typeof obj === "object" || typeof obj === "function" ?
        class2type[Object.prototype.toString.call(obj)] || "object" :
        typeof obj;
}

console.log(type(true))