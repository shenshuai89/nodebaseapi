let Circle = function () {};
let circle = new Circle();
let shape = Object.create(null);
// 设置该对象的原型链引用
// 给shape设置[[Prototype]]
// 作用类似传统的shape.__proto__ = circle,传统操作过时且不推荐使用的
Reflect.setPrototypeOf(shape, circle);
console.log(Reflect.getPrototypeOf(shape) === circle);

let Box = function () {};
let p = {
  a: function () {
    console.log("aaa");
  },
};
Reflect.setPrototypeOf(Box.prototype, p);
let box = new Box();
box.a();
console.log(Reflect.getPrototypeOf(box) === Box.prototype);
