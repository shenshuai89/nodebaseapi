// http2必须依赖https协议
// openssl 生成电子签名
// openssl req -newkey rsa:2048 -nodes -keyout res_private.key -x509 -days 365 -out cert.crt
const http2 = require("http2");
const path = require("path");
const fs = require("fs");
const { HTTP2_HEADER_PATH, HTTP2_HEADER_STATUS } = http2.constants;
const server = http2.createSecureServer({
  cert: fs.readFileSync(path.resolve(__dirname, "./cert.crt")),
  key: fs.readFileSync(path.resolve(__dirname, "./res_private.key")),
});
const staticDir = path.join(__dirname, "static");
server.on("stream", async (stream, headers) => {
  let requestPath = headers[HTTP2_HEADER_PATH];
  if (requestPath == "/") {
    requestPath = "/index.html";
    let dirs = fs.readdirSync(staticDir);
    dirs.forEach((dir) => {
      let pushPath = path.join(staticDir, dir);
      // http2 主动推送资源
      stream.pushStream(
        { [HTTP2_HEADER_PATH]: "/" + dir },
        (err, pushStream) => {
          fs.createReadStream(pushPath).pipe(pushStream);
        }
      );
    });
    stream.respondWithFile(path.join(__dirname, requestPath), {
      "Content-Type": "text/html",
    });
  } else {
    stream.respond({
      [HTTP2_HEADER_STATUS]: 404,
    });
    stream.end("not found");
  }
});
server.listen(3002, () => {
  console.log("http2 server run port: 3002");
});
