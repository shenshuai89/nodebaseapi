const { EventEmitter } = require("events");
class CustomEventEmitter extends EventEmitter{

}
const coe = new CustomEventEmitter;

coe.once('test', () => {
    console.log('emit once test');
})
setInterval(() =>{
    coe.emit('test')
}, 1000)