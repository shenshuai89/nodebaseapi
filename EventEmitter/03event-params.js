const { EventEmitter } = require("events");
class CustomEventEmitter extends EventEmitter{

}
const coe = new CustomEventEmitter;
coe.on('error', function(err){
    console.log(err);
})
coe.emit('error', new Error('event error'), Date.now());
