const EventEmitter = require('events').EventEmitter;
class CustomEventEmitter extends EventEmitter {

}
const coe = new CustomEventEmitter();
coe.on('test', function(){
    console.log('test coe');
})

setInterval(() => {
    coe.emit('test')
}, 1000);