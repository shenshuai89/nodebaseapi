const { EventEmitter } = require("events");
class CustomEventEmitter extends EventEmitter {}
const coe = new CustomEventEmitter();

const fn1 = () => {
  console.log("fn1");
};
const fn2 = () => {
  console.log("fn2");
};

coe.on("test", fn1);
coe.on("test", fn2);

setInterval(() => {
  coe.emit("test");
}, 200);

// removeListener移除绑定的单个事件
// removeAllListeners移动绑定的所有事件
setTimeout(() => {
//   coe.removeListener("test", fn1);
    coe.removeAllListeners("test")
}, 1000);

setTimeout(() => {
  coe.removeListener("test", fn2);
}, 2000);
