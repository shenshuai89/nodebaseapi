/* 
“信息内容签名”
数字签名主要是保证信息的完整和提供信息发送者的身份认证和不可抵赖性，这其中，
“完整性”主要就是由报文摘要提供的，报文摘要就是用来防止发送的报文被篡改。
用RSA私钥进行签名，一般是16进制字符串
用公钥验证签名的准确性。
*/
const fs = require("fs");
const path = require("path");
const crypto = require("crypto");
// 测试使用
const otherkeys = require("./otherkeys.js");

const privatePem = fs.readFileSync(
  path.resolve(__dirname, "../privatekey.pem")
);
const publicPem = fs.readFileSync(path.resolve(__dirname, "../publickey.pem"));
const privatekey = privatePem.toString();
const publickey = publicPem.toString();
const data = "我的账号有10000000W";
const sign = crypto.createSign("RSA-SHA256");
sign.update(data);
const sig = sign.sign(privatekey, "hex"); //得到对data的加密数据
console.log("data的数字签名值：" + sig);

// 验证数字签名的正确性
const verify = crypto.createVerify("RSA-SHA256");
verify.update(data);
// const t = verify.verify(publickey, sig, "hex");
const othert = verify.verify(otherkeys.pubKey, sig, "hex");
// console.log("非对称数字签名 校验结果结果：" + t);
console.log("非对称数字签名 校验结果结果：" + othert);
