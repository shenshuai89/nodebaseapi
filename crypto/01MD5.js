const crypto = require("crypto");
let md5 = crypto.createHash("md5");
// update 相当于追加加密内容
/* md5.update("hello");
md5.update("world"); */
md5.update("helloworld");
console.log("md5加密结果：" + md5.toString());
const md5c = md5.digest("hex"); //hex十六进制
console.log("md5加密结果：" + md5c);
