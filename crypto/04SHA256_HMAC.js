const crypto = require("crypto");
// HMAC是密钥相关的哈希运算消息认证码，HMAC运算利用哈希算法，以一个密钥和一个消息为输入，生成一个消息摘要作为输出。
const secret = "1qazXDR%";
let hmac = crypto.createHmac("sha256", secret);
const content = hmac.update("helloworld");
const contentCrypto = content.digest("hex");
console.log("sha256[Hmac]加密后的结果： " + contentCrypto);
