const crypto = require("crypto");

/* 
    createCipheriv方法接受三个参数：algorithm用于指定加密算法，如aes-128-ccm、aes-128-cbc等；【密码模式。 'cbc'、'ccm'、'cfb'、'ctr'、'ecb'、'gcm'、'ocb'、'ofb'、'stream'、'wrap'、'xts' 之一。】
    key是用于加密的密钥；iv参数可选，用于指定加密时所用的向量。
    注意这里的密钥必须是8/16/32位，如果加密算法是128，则对应的密钥是16位，
    如果加密算法是256，则对应的密钥是32位。
*/
// AES 对称加密, secret IV 必须是16位
const secret = "1qazXDR%%TGBnji9";
const IV = "0123456789abcdef";

const content = "helloworld";
const cipher = crypto.createCipheriv("aes-128-cbc", secret, IV); //使用aes128加密
// const enc = cipher.update(content, "utf8", "hex") + cipher.final("hex"); //编码方式转为hex;
const enc = cipher.update(content, "utf8", "base64") + cipher.final("base64"); //编码方式转为base64;
console.log("AES对称加密结果：", enc);
// AES 对称解密
const decipher = crypto.createDecipheriv("aes-128-cbc", secret, IV);
// const dec = decipher.update(enc, "hex", "utf8") + decipher.final("utf8");
const dec = decipher.update(enc, "base64", "utf8") + decipher.final("utf8");
console.log("AES对称解密结果：" + dec);
