let AipImageClassifyClient = require('baidu-aip-sdk').imageClassify;
// https://console.bce.baidu.com/ai/?fromai=1#/ai/imagerecognition/app/list
var APP_ID = "";
var API_KEY = "";
var SECRET_KEY = "";
var client = new AipImageClassifyClient(APP_ID, API_KEY, SECRET_KEY);
let fs = require('fs');

let image = fs.readFileSync('./che.jpg').toString('base64');

// 调用通用物体识别
client.advancedGeneral(image).then(function(result) {
    console.log(JSON.stringify(result));
}).catch(function(err) {
    // 如果发生网络错误
    console.log(err);
});

// 如果有可选参数
let options = {};
options['baike_num'] = '5';

// 带参数调用通用物体识别
client.advancedGeneral(image, options).then(function(result) {
    console.log(JSON.stringify(result));
}).catch(function(err) {
    // 如果发生网络错误
    console.log(err);
});