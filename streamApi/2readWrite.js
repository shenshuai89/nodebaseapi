let fs = require("fs");
let path = require("path");

// ReadStream : on[data], on[end]
// writeStream: write, end

let rs = fs.createReadStream(path.resolve(__dirname, "x.txt"), {
  highWaterMark: 2,
});

let ws = fs.createWriteStream(path.resolve(__dirname, "xy.txt"), {
  highWaterMark: 3,
});

let arr = [];
// 边读边写
rs.on("data", function (data) {
  arr.push(data);
  // console.log("read", arr);
  // 写入数据操作， flag仅代表是否有空闲内存，不表示是否写入成功
  let flag = ws.write(data);
  console.log(flag);
});

rs.on("end", function () {
  console.log(Buffer.concat(arr).toString());
  ws.end();
});
