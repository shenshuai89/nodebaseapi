const fs = require("fs");
const path = require("path");
const ReadStream = require("./ReadStream");
const WriteStream = require("./WriteStream");

// fs.createReadStream(path.resolve(__dirname, "x.txt")).pipe(
//   fs.createWriteStream(path.resolve(__dirname, "copy.txt"))
// );

let rs = new ReadStream(path.resolve(__dirname, "x.txt"), { highWaterMark: 4 });
let ws = new WriteStream(path.resolve(__dirname, "copy.txt"), {
  highWaterMark: 1,
});
// rs.on("data", (data) => {
//   // console.log(data);
//   let flag = ws.write(data);
//   if (!flag) {
//     rs.pause();
//   }
// });

// ws.on("drain", () => {
//   rs.resume();
// });
rs.pipe(ws);
