const fs = require("fs");
const path = require("path");

const ReadSteam = require("./ReadStream");

const rs = new ReadSteam(path.join(__dirname, "x.txt"), {
  flags: "r",
  encoding: null,
  highWaterMark: 3,
  mode: 0o666,
  autoClose: true,
  start: 0,
  end: 10,
});
rs.on("open", function () {
  console.log("opening");
});
let arr = [];
rs.on("data", function (data) {
  console.log(data);
  arr.push(data);
  rs.pause();
});
setInterval(function () {
  rs.resume();
}, 1000);
rs.on("end", function (data) {
  console.log(Buffer.concat(arr).toString());
});
rs.on("error", function (err) {
  console.log(err);
});
rs.on("close", function (err) {
  console.log(err);
});
