let fs = require("fs");
let path = require("path");
let WriteStream = require("./WriteStream.js");

let ws = new WriteStream(path.resolve(__dirname, "xy.txt"), {
  highWaterMark: 3,
});

// let ws = fs.createWriteStream(path.resolve(__dirname, "xy.txt"), {
//   highWaterMark: 3,
// });
let index = 0;
// 没有读数据过程，直接持续写数据
function write() {
  let flag = true;
  while (flag && index < 10) {
    flag = ws.write(index + "");
    console.log(flag);
    index++;
  }
  if (index > 9) {
    ws.end("end");
  }
}
// write的数据清空了highWaterMark设置的大小空间，会触发drain事件
ws.on("drain", function () {
  console.log("空空空");
  write();
});
write();

// 关闭写入流
ws.on("close", function () {
  console.log("close");
});
